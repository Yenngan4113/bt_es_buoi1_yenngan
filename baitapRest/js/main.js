class Student {
  constructor(...other) {
    return other;
  }
}
tinhDiemTB = (arr) => {
  let diemTB = 0;
  let Tong = 0;
  arr.forEach((item) => {
    Tong += item;
  });
  diemTB = Tong / arr.length;
  return diemTB;
};

document.querySelector("#btnKhoi1").addEventListener("click", () => {
  let diemToan = +document.querySelector("#inpToan").value;
  let diemLy = +document.querySelector("#inpLy").value;
  let diemHoa = +document.querySelector("#inpHoa").value;
  if (diemHoa != 0 && diemToan != 0 && diemLy != 0) {
    let scoreClass1 = new Student(diemToan, diemLy, diemHoa);
    let diemTB = tinhDiemTB(scoreClass1);
    let diemTBFormated = (Math.round(diemTB * 100) / 100).toFixed(2);
    document.querySelector("#tbKhoi1").innerHTML = `${diemTBFormated}`;
  } else {
    document.querySelector("#tbKhoi1").innerHTML = NaN;
  }
});
document.querySelector("#btnKhoi2").addEventListener("click", () => {
  let diemVan = +document.querySelector("#inpVan").value;
  let diemSu = +document.querySelector("#inpSu").value;
  let diemDia = +document.querySelector("#inpDia").value;
  let diemAnh = +document.querySelector("#inpEnglish").value;
  if (diemVan != 0 && diemSu != 0 && diemDia != 0 && diemAnh != 0) {
    let scoreClass2 = new Student(diemVan, diemSu, diemDia, diemAnh);
    let diemTB = tinhDiemTB(scoreClass2);
    let diemTBFormated = (Math.round(diemTB * 100) / 100).toFixed(2);
    document.querySelector("#tbKhoi2").innerHTML = `${diemTBFormated}`;
  } else {
    document.querySelector("#tbKhoi2").innerHTML = NaN;
  }
});
