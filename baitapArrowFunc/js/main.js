const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
let renderColors = () => {
  let contentHTML = "";
  for (let i = 0; i < colorList.length; i++) {
    let contentButton = `<button class="color-button ${colorList[i]}" onClick="changeColor('${colorList[i]}',${i})" ></button>`;
    contentHTML += contentButton;
  }
  document.querySelector("#colorContainer").innerHTML = contentHTML;
};
renderColors();

const changeColor = (color, i) => {
  document.querySelector("#house").className = `house ${color}`;
  let btns = document.querySelectorAll(".color-button");
  btns.forEach((button) => {
    button.classList.remove("active");
  });
  btns[i].classList.add("active");
};
