let string = document.querySelector(".heading").innerText;
let char = [...string];
char.splice(5, 1);
console.log(char);
const showChar = () => {
  let contentHTML = "";
  char.forEach((item) => {
    let innerSpanTag = /*html*/ `<span class="rotate">${item}</span>`;
    contentHTML += innerSpanTag;
  });
  document.querySelector(".showChar").innerHTML = contentHTML;
};
showChar();
